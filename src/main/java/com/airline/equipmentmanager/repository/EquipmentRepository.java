package com.airline.equipmentmanager.repository;

import com.airline.equipmentmanager.entity.Equipment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EquipmentRepository extends JpaRepository<Equipment, Long> {
}
