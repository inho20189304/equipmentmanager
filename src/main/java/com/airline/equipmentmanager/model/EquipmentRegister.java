package com.airline.equipmentmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EquipmentRegister {
    private String equipmentName;
    private LocalDate purchaseDate;
    private Integer price;
}
