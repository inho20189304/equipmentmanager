package com.airline.equipmentmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Equipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long id;
    @Column(nullable = false, length = 20)
    private String equipmentName;
    @Column(nullable = false)
    private LocalDate purchaseDate;
    @Column(nullable = false)
    private Integer price;
}
