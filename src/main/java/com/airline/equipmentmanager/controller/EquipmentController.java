package com.airline.equipmentmanager.controller;

import com.airline.equipmentmanager.model.EquipmentRegister;
import com.airline.equipmentmanager.service.EquipmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(("/v1/equipment"))
public class EquipmentController {
    private final EquipmentService equipmentService;

    public String setEquipment(@RequestBody @Valid EquipmentRegister register) {
        equipmentService.setEquipment(register);

        return "ok";
    }
}
