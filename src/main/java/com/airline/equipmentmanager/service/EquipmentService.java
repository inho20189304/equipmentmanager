package com.airline.equipmentmanager.service;

import com.airline.equipmentmanager.entity.Equipment;
import com.airline.equipmentmanager.model.EquipmentRegister;
import com.airline.equipmentmanager.repository.EquipmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EquipmentService {
    private final EquipmentRepository equipmentRepository;

    public void setEquipment(EquipmentRegister register) {
        Equipment addData = new Equipment();
        addData.setEquipmentName(register.getEquipmentName());
        addData.setPurchaseDate(register.getPurchaseDate());
        addData.setPrice(register.getPrice());

        equipmentRepository.save(addData);
    }
}
